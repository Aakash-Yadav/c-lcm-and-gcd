#include <iostream>
using std::cout; 
using std::endl ; 
using std::cin;
double gcd(int,int);

int lcm(int x , int y){
    double value = gcd(x,y);
    return (x*y)/gcd(x,y);
    }

double gcd(int num1 , int num2 ){
    if (num2==0){
        return num1 ;
    }
    else{
        return gcd(num2,num1%num2);
    }
}
int main()
{int x,y;
    cout <<"Enter The Number1: ";
    cin >> x;
    cout <<"Enter The Number2: ";
    cin >>y;
    cout <<endl; 
    cout << "The Greatest Common Denominator of "<<x<<" and "<<y<<" is "<<gcd(x,y)<<endl;
    cout <<endl; 
    cout << "The Least Common Multiple of "<<x<<" and "<<y<<" is "<<lcm(x,y)<<endl;
    cout <<endl;}