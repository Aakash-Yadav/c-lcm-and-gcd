## Least common multiple
The Least Common Multiple (LCM) of two integers a and b, usually denoted by LCM (a, b), is the smallest positive integer that is divisible by both a and b. In simple words, the smallest positive number that is a multiple of two or more numbers is the LCM of those two numbers. Check out the LCM formula for any two numbers and for fractions using GCD (HCF)

#### Example

![alt text](https://xp.io/storage/2Cv5CXPY.png)

## Greatest common divisor

The greatest common divisor (GCD) of two or more numbers is the greatest common factor number that divides them, exactly. It is also called the highest common factor (HCF).
#### Example
![alt text](https://xp.io/storage/2CveKWlC.png)

## really I Got 23/25  marks
###### thank you
